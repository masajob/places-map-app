function filter(org,filterOptions){

    let newOrg = org;

    let placeCategory = filterOptions.placeCategory,
        karaoke = filterOptions.karaoke,
        childrenRoom = filterOptions.childrenRoom,
        program = filterOptions.program,
        banquet = filterOptions.banquet,
        reservation = filterOptions.reservation,
        companyAction = filterOptions.companyAction,
        valueSlider = filterOptions.valueSlider,
        valuePeople = filterOptions.valuePeople;

    // filter 1 - category
    newOrg = newOrg.filter((item) => {

        if(placeCategory === 6) return item;
        if(placeCategory !== item.category) return;

        return item;

    });

    // filter 2 - visible all or free
    newOrg = newOrg.filter((item) => {

        return item;

    });

    // filter 3 - dop
    newOrg = newOrg.filter((item) => {

        if( !karaoke && !childrenRoom && !program && !banquet && !reservation && !companyAction){
            return item;
        }

        if(karaoke === true && item.karaoke === false) return;
        if(childrenRoom === true && item.childrenRoom === false) return;
        if(program === true && item.program === false) return;
        if(banquet === true && item.banquet === false) return;
        if(reservation === true && item.reservation === false) return;
        if(companyAction === true && item.companyAction === false) return;

        return item;

    });

    // filter 4 - price
    newOrg = newOrg.filter((item) => {

        if(valueSlider.min <= item.priceFrom && valueSlider.max >= item.priceFrom) return item;

    });

    // filter 5 - count people
    newOrg = newOrg.filter((item) => {

        if(valuePeople === null) return item;

        let all = item.allPlaces,
            reserved = item.reservedPlaces;

        let places = all - reserved;

        if(valuePeople <= places) return item;

    });

    return newOrg;

}
export default filter;