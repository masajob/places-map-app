let arr = [];

const setMarker = (organization) => {
    if(arr && arr.length > 0){
        for (var i = 0; i < arr.length; i++) {
            arr[i].setMap(null);
        }
        arr = [];
    }

    let timeout = 0;

    organization.map((item) => {
        setTimeout(() => {
            let marker = new window.google.maps.Marker({
                position: item.position,
                map: window.gmaps['map1'].gmap,
                animation: window.google.maps.Animation.DROP,
                title: 'Кликните, что бы узнать больше'
            });
            arr.push(marker);

            let rangePlace;
            let places = item.reservedPlaces/item.allPlaces;
            if(places <= 0.7){
                marker.setIcon('img/marker_2.png');
                rangePlace =  '<div class="progress-content">' +
                                '<div class="progress"><div class="grayback grayback-2"></div><div class="strips"></div></div>' +
                                '<div class="progress-text">Мест много</div>' +
                              '</div>'

            } else if(places > 0.7 && places < 1){
                marker.setIcon('img/marker_3.png');
                rangePlace =  '<div class="progress-content">' +
                                '<div class="progress"><div class="grayback"></div><div class="strips"></div></div>' +
                                '<div class="progress-text">Места ещё есть</div>' +
                              '</div>'
            } else if(places >= 1){
                marker.setIcon('img/marker_1.png');
                rangePlace =  '<div class="progress-content">' +
                    '<div class="progress progress-2"><div class="grayback grayback-3"></div><div class="strips"></div></div>' +
                    '<div class="progress-text">Мест нет</div>' +
                    '</div>'
            }


            let contentString = '<div id="content">'+
                '<div class="info-window-img-box"><div class="info-window-img"><img src="' + item.img +'"/></div></div>'+
                '<div class="info-window-content">'+ item.name + '</div>'+
                '<div class="info-window-price">Цена за вечер: от <span><b>' + item.priceFrom + ' руб.</b></span></div>' +
                rangePlace +
                '</div>';

            let infowindow = new window.google.maps.InfoWindow({
                content: contentString,
                disableAutoPan: false
            });

            marker.addListener('mouseover', function() {
                infowindow.open(window.gmaps['map1'].gmap, marker);
            });

            marker.addListener('mouseout', function() {
                infowindow.close();
            });

            marker.addListener('click', function() {
                window.document.querySelector('.filter').classList.add('active');
                window.document.querySelector('.more-info').classList.add('active');
            });

        },timeout);

        timeout += 500;
    });

    return arr;

};

export default setMarker;