import React, { Component } from 'react';

class InfoWindowContent extends Component {

    getProgressPlace = (all, reserved) => {
        let places = reserved/all;
        if(places <= 0.6){
            return (
                    <div className="progress-content">
                        <div className="progress">
                            <div className="grayback grayback-2"></div>
                            <div className="strips"></div>
                        </div>
                        <div className="progress-text">Мест много</div>
                    </div>
            )
        } else if(places > 0.6 && places < 1){
            return (
                <div className="progress-content">
                    <div className="progress">
                        <div className="grayback"></div>
                        <div className="strips"></div>
                    </div>
                    <div className="progress-text">Места ещё есть</div>
                </div>
            )
        } else if(places >= 1){
            return (
                <div className="progress-content">
                    <div className="progress progress-2">
                        <div className="grayback grayback-3"></div>
                        <div className="strips "></div>
                    </div>
                    <div className="progress-text">Мест нет</div>
                </div>
            )
        }
    };

    render(){
        return (
            <div id="content">
                <div className="info-window-img-box">
                    <div className="info-window-img">
                        <img src={this.props.organization.img}/>
                    </div>
                </div>
                <div className="info-window-content">{this.props.organization.name}</div>
                <div className="info-window-address">{this.props.organization.address}</div>
                <div className="info-window-price">
                    Стоимость/чел.: от {this.props.organization.priceFrom} руб.
                </div>
                <div className="info-services">
                    { this.props.organization.karaoke && <div className="info-service-item"><span>✪</span>Караоке</div> }
                    { this.props.organization.childrenRoom && <div className="info-service-item"><span>✪</span>Детская комната</div> }
                    { this.props.organization.program && <div className="info-service-item"><span>✪</span>Программа мероприятий</div> }
                    { this.props.organization.banquet && <div className="info-service-item"><span>✪</span>Банкет</div> }
                    { this.props.organization.reservation && <div className="info-service-item"><span>✪</span>Резерв столиков</div> }
                    { this.props.organization.companyAction && <div className="info-service-item"><span>✪</span>Для компании</div> }
                </div>
                {this.getProgressPlace(this.props.organization.allPlaces,this.props.organization.reservedPlaces)}
            </div>
        )
    }
}
export default InfoWindowContent;