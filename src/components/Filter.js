import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import RaisedButton from 'material-ui/RaisedButton';
import DatePicker from 'material-ui/DatePicker';
import TextField from 'material-ui/TextField';
import InputRange from './js/index';

import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

let DateTimeFormat = global.Intl.DateTimeFormat;

const persons = [
    {value: 0, name: 'Oliver Hansen'},
    {value: 1, name: 'Van Henry'},
    {value: 2, name: 'April Tucker'},
    {value: 3, name: 'Ralph Hubbard'},
    {value: 4, name: 'Omar Alexander'},
    {value: 5, name: 'Carlos Abbott'},
    {value: 6, name: 'Miriam Wagner'},
    {value: 7, name: 'Bradley Wilkerson'},
    {value: 8, name: 'Virginia Andrews'},
    {value: 9, name: 'Kelly Snyder'},
];

const style = {
    margin: 12,
    width: 150,
    height: 45
};

const styleDatePicker = {

};

const muiTheme = getMuiTheme({
    palette: {
        primary1Color: '#050408',
        accent1Color: 'red',
    }
});

class Filter extends Component {

    state = {
        value: 1,
        typePlace: 6,
        services: [
            {
                id: 1,
                selected: false,
                name: 'Караоке'
            }, {
                id: 2,
                selected: false,
                name: 'Детская комната'
            }, {
                id: 3,
                selected: false,
                name: 'Программа мероприятий'
            }, {
                id: 4,
                selected: false,
                name: 'Банкет'
            }, {
                id: 5,
                selected: false,
                name: 'Резерв столиков'
            }, {
                id: 6,
                selected: false,
                name: 'Для компании'
            }
        ],
        valueSlider: {
            min: 2000,
            max: 7000
        }
    };

    handleChange = (event, index, value) => this.setState({value});

    setTypePlace = (val) => {
        this.setState({ typePlace: val });
        this.props.state.filter.placeCategory = val;
    };

    setServices = (val) => {
        let services = this.state.services.map((item) => {
            if(item.id === val) item.selected = !item.selected;
            return item;
        });

        this.setState({ services: services });

        services.forEach((item) => {
            if(item.id === 1) { this.props.state.filter.karaoke = item.selected }
            if(item.id === 2) { this.props.state.filter.childrenRoom = item.selected }
            if(item.id === 3) { this.props.state.filter.program = item.selected }
            if(item.id === 4) { this.props.state.filter.banquet = item.selected }
            if(item.id === 5) { this.props.state.filter.reservation = item.selected }
            if(item.id === 6) { this.props.state.filter.companyAction = item.selected }
        });
    };

    changeSlider = (val) => {
        this.setState({ valueSlider: val });
        this.props.state.filter.valueSlider = this.state.valueSlider;
    };

    changeInput = (e, val) => {
        if(val === ''){
            this.props.state.filter.valuePeople = null;
            return;
        }
        this.props.state.filter.valuePeople = +val;
    };

    render(){
        return (
            <div className={ this.props.state.isMoreInfo ? "filter active" : "filter"}>
                <div className="filter-content">

                        <h1>Места для праздников</h1>

                        <div className="content-box">

                            <div className="place-choice">
                                <div className="filter-title">Тип заведения</div>
                                <div className="filter-type-place">
                                    <div className={ this.state.typePlace === 1 ? "item-type-place active" : "item-type-place" }
                                         onClick={() => this.setTypePlace(1)}
                                    >Ресторан, кафе, бар</div>
                                    <div className={ this.state.typePlace === 2 ? "item-type-place active" : "item-type-place" }
                                         onClick={() => this.setTypePlace(2)}
                                    >Клуб</div>
                                    <div className={ this.state.typePlace === 3 ? "item-type-place active" : "item-type-place" }
                                         onClick={() => this.setTypePlace(3)}
                                    >База отдыха, пансионат</div>
                                    <div className={ this.state.typePlace === 4 ? "item-type-place active" : "item-type-place" }
                                         onClick={() => this.setTypePlace(4)}
                                    >Сауна</div>
                                    <div className={ this.state.typePlace === 5 ? "item-type-place active" : "item-type-place" }
                                         onClick={() => this.setTypePlace(5)}
                                    >Отель, гостиница</div>
                                    <div className={ this.state.typePlace === 6 ? "item-type-place active" : "item-type-place" }
                                         onClick={() => this.setTypePlace(6)}
                                    >Все</div>
                                </div>
                            </div>

                            <div className="place-choice">
                                <div className="filter-title">Мероприятие</div>
                                <div className="filter-action">
                                    <div className="select-action">
                                        <MuiThemeProvider muiTheme={muiTheme}>
                                            <SelectField value={this.state.value} onChange={this.handleChange}>
                                                <MenuItem value={1} label="Новый год" primaryText="Новый год" />
                                                <MenuItem value={2} label="Новогодний корпоратив" primaryText="Новогодний корпоратив" />
                                                <MenuItem value={3} label="8 Марта" primaryText="8 Марта" />
                                                <MenuItem value={4} label="Любое" primaryText="Любое"/>
                                            </SelectField>
                                        </MuiThemeProvider>
                                    </div>
                                    <div className="date-action">
                                        <MuiThemeProvider muiTheme={muiTheme}>
                                            <DatePicker
                                                floatingLabelText="Дата"
                                                defaultDate={new Date(2017,11,31)}
                                                disableYearSelection={true}
                                                style={styleDatePicker}
                                                className="one"
                                            />
                                        </MuiThemeProvider>
                                    </div>
                                </div>
                            </div>

                            <div className="place-choice">
                                <div className="filter-title">Дополнительно</div>
                                <div className="filter-type-place">
                                    {
                                        this.state.services.map((item) => {
                                            return (
                                                <div key={item.id}
                                                     className={ item.selected ? "item-type-place active" : "item-type-place" }
                                                     onClick={ () => this.setServices(item.id) }
                                                >
                                                    {item.name}
                                                </div>
                                            )
                                        })
                                    }
                                </div>
                            </div>

                            <div className="price-and-count">

                                <div className="block-range-price">
                                    <div className="filter-title">Стоимость на человека, руб.</div>
                                    <InputRange
                                        draggableTrack
                                        maxValue={10000}
                                        minValue={0}
                                        onChange={value => this.changeSlider(value)}
                                        onChangeComplete={value => console.log(value)}
                                        value={this.state.valueSlider}
                                    />
                                </div>

                                <div className="block-count-people">
                                    <div className="filter-title">Количество человек</div>
                                    <div className="text-field-count">
                                        <MuiThemeProvider>
                                            <TextField
                                                hintText="Количество"
                                                onChange={(e,val) => this.changeInput(e,val)}
                                                type="number"
                                                min="0"
                                            />
                                        </MuiThemeProvider>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div className="filter-search">
                            <div className="search-btn">
                                <MuiThemeProvider muiTheme={muiTheme}>
                                    <RaisedButton
                                        onClick={this.props.clickFilter}
                                        label="Подобрать"
                                        primary={true}
                                        style={style}
                                    />
                                </MuiThemeProvider>
                            </div>
                            <div className="search-count">
                                Найдено: <span className="search-number">
                                {this.props.state.filter.countPlaces}
                                </span>
                            </div>
                        </div>

                </div>
            </div>
        );
    }
}
export default Filter;