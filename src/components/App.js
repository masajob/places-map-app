import React, { Component } from 'react';
import {Map, InfoWindow, Marker, GoogleApiWrapper} from 'google-maps-react';

import organization from '../data/data';
import mapOptions from '../settings/settingsMap';
import filter from '../logic/filter';

import InfoOrganization from '../components/InfoOrganization';
import Filter from '../components/Filter';
import InfoWindowContent from './InfoWindowContent';

import './css/index.css';
import './css/style.css';

class AppContainer extends Component {

    constructor(props){
        super(props);
        this.state = {
            activeMarker: {},
            showingInfoWindow: false,
            dataInfoWindow: {},
            dataMoreInfo: {},
            organization: organization,
            isMoreInfo: false,
            moreInfoOrganization: {},
            mapOptions: mapOptions,
            renderItemsNum: 0,
            filter: {
                placeCategory: 6, // 1,2,3,4,5,6
                countPlaces: 33,
                karaoke: false,
                childrenRoom: false,
                program: false,
                banquet: false,
                reservation: false,
                companyAction: false,
                valueSlider: { min: 2000, max: 7000 },
                valuePeople: null
            }
        };
    }

    clickFilter = () => {

        let newDataOrganization = filter(organization,this.state.filter);
        let newFilter = Object.assign({},this.state.filter,{ countPlaces: newDataOrganization.length });
        let newMapOption;

        if(this.state.filter.placeCategory === 3){
            newMapOption = Object.assign({},this.state.mapOptions, {zoom: 10, center: { lat: 56.319304, lng: 43.931284 }})
        } else {
            newMapOption = Object.assign({},this.state.mapOptions, {zoom: 14, center: { lat: 56.323013, lng: 43.996795 }})
        }

        this.setState({
            organization: newDataOrganization,
            renderItemsNum: 0,
            filter: newFilter,
            mapOptions: newMapOption
        });
    };

    leaveInfoOrganization = () => {
        this.setState({
            isMoreInfo: false
        });
    };

    onMarkerClick = (props,marker) => {

        let dataMoreInfo;
        organization.map((item) => {
            if(item.id === props.id){
                dataMoreInfo = item;
            }
        });

        this.setState({
            isMoreInfo: true,
            dataMoreInfo: dataMoreInfo
        });
    };

    onMarkerOver = (props,marker) => {

        let dataInfoWindow;
        organization.map((item) => {
            if(item.id === props.id){
                dataInfoWindow = item;
            }
        });

        this.setState({
            activeMarker: marker,
            showingInfoWindow: true,
            dataInfoWindow: dataInfoWindow
        });
    };

    onMarkerOut = () => {
        this.setState({
            activeMarker: {},
            showingInfoWindow: false
        });
    };

    getMarkerIcon = (all,reserved,spec) => {
        let places = reserved/all;
        if(places < 0.4){
            if(!spec) return 'img/marker_green_small.png';
            if(spec) return 'img/marker_green_vip.png';
        } else if(places >= 0.4 && places < 0.7){
            if(!spec) return 'img/marker_yellow_small.png';
            if(spec) return 'img/marker_yellow_vip.png';
        } else if(places >= 0.7 && places < 0.9){
            if(!spec) return 'img/marker_orange_small.png';
            if(spec) return 'img/marker_orange_vip.png';
        } else if(places >= 0.9 && places < 1){
            if(!spec) return 'img/marker_red_small.png';
            if(spec) return 'img/marker_red_vip.png';
        } else if(places >= 1){
            if(!spec) return 'img/marker_grey_small.png';
            if(spec) return 'img/marker_gray_vip.png';
        }
    };

    componentWillMount = () => {
        this.state.organization = filter(organization,this.state.filter);
    };

    render() {

        let markers = this.state.organization,
            renderItemsNum = this.state.renderItemsNum,
            rendering = markers.length > renderItemsNum;

        if(rendering){
            setTimeout(() => this.setState({
                renderItemsNum: ++renderItemsNum
            }),100);
        }

        return (
            <div className="container">

                <div className="map-box">
                    <Map google={this.props.google} { ...this.state.mapOptions } id="my-map">

                        {
                            this.state.organization.slice(0, renderItemsNum).map((item) => {
                                return (
                                    <Marker
                                        key = {item.id}
                                        id = {item.id}
                                        position = {item.position}
                                        title = {'Кликните, что бы узнать больше'}
                                        onMouseover = {this.onMarkerOver}
                                        onMouseout = {this.onMarkerOut}
                                        onClick = {this.onMarkerClick}
                                        icon = {this.getMarkerIcon(item.allPlaces,item.reservedPlaces,item.spec)}
                                    />
                                );
                            })
                        }

                        <InfoWindow
                            marker={this.state.activeMarker}
                            visible={this.state.showingInfoWindow}>

                            <InfoWindowContent organization={this.state.dataInfoWindow} />

                        </InfoWindow>

                    </Map>
                    <div className="logo">
                        <a href="/">
                            <img src="./img/logo.png" alt="logo"/>
                        </a>
                    </div>
                    <div className="banner-info">
                        <a href="/">
                            <img src="./img/adv.png" alt="adv"/>
                        </a>
                    </div>
                </div>

                <div className="left-box">

                    <Filter
                        organization={organization}
                        clickFilter={this.clickFilter}
                        state={this.state}
                    />

                    {
                        this.state.isMoreInfo ?
                            <InfoOrganization state={this.state} leaveInfoOrganization={this.leaveInfoOrganization}/> :
                            <InfoOrganization state={this.state} leaveInfoOrganization={this.leaveInfoOrganization}/>
                    }

                </div>
            </div>

        );
    }
}

const App = GoogleApiWrapper({
    apiKey: mapOptions.apiKey
})(AppContainer);

export default App;