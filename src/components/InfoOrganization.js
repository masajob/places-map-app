import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

const style = {
    margin: 12,
    width: 200,
    height: 45
};

const style2 = {
    margin: 12,
    width: 150,
    height: 45
};

const muiTheme = getMuiTheme({
    palette: {
        primary1Color: '#050408',
        accent1Color: '#050408',
    }
});

class InfoOrganization extends Component {

    state = {
        open: false,
    };

    handleOpen = () => {
        this.setState({open: true});
    };

    handleClose = () => {
        this.setState({open: false});
    };

    infoGetOut = () => {
        this.props.setState({
            isMoreInfo: false
        });
        // this.props.state.isMoreInfo = false;
    };

    render(){

        const actions = [
            <FlatButton
                label="Отмена"
                primary={true}
                onClick={this.handleClose}
            />,
            <FlatButton
                label="Забронировать"
                primary={true}
                onClick={this.handleClose}
            />,
        ];

        return (
            <div className={ this.props.state.isMoreInfo ? "more-info active" : "more-info"}>
                <div className="more-info-content">
                    <div className="more-info-content-box">
                        <h1 className="more-info-head">{this.props.state.dataMoreInfo.name}</h1>
                        <div className="more-info-address">{this.props.state.dataMoreInfo.address}</div>
                        <img src={this.props.state.isMoreInfo ?
                            this.props.state.dataMoreInfo.img :
                            this.props.state.dataInfoWindow.img
                        }
                             className="more-info-img"
                        />
                        <div className="more-info-title">
                            <h1>Контакты</h1>
                            <div className="more-info-phone">Телефон: +7 831 2-333-555</div>
                            <div className="more-info-web">Сайт: <a href="/">www.organization.com</a></div>
                            <div className="more-info-email">E-mail: organization@gmail.com</div>
                        </div>

                        <div className="more-price-and-count">
                            <div className="more-info-title more-info-title-margin">
                                <h1>Цена</h1>
                                <div className="more-info-phone">
                                    от {this.props.state.dataMoreInfo.priceFrom} руб.
                                </div>
                            </div>
                            <div className="more-info-title">
                                <h1>Свободных мест</h1>
                                <div className="more-info-phone more-info-phone-center">
                                    {this.props.state.dataMoreInfo.allPlaces - this.props.state.dataMoreInfo.reservedPlaces}
                                </div>
                            </div>
                        </div>

                        <div className="more-info-title">
                            <h1>Есть</h1>
                            <div className="more-services">
                                { this.props.state.dataMoreInfo.karaoke && <div className="more-service-item"><span>✪</span>Караоке</div> }
                                { this.props.state.dataMoreInfo.childrenRoom && <div className="more-service-item"><span>✪</span>Детская комната</div> }
                                { this.props.state.dataMoreInfo.program && <div className="more-service-item"><span>✪</span>Программа мероприятий</div> }
                                { this.props.state.dataMoreInfo.banquet && <div className="more-service-item"><span>✪</span>Банкет</div> }
                                { this.props.state.dataMoreInfo.reservation && <div className="more-service-item"><span>✪</span>Резерв столиков</div> }
                                { this.props.state.dataMoreInfo.companyAction && <div className="more-service-item"><span>✪</span>Для компании</div> }
                            </div>
                        </div>

                    </div>

                    <div className="infoBtnPanel">

                        <MuiThemeProvider>
                            <RaisedButton
                                onClick={this.props.leaveInfoOrganization}
                                label="Вернуться к фильтру"
                                primary={true}
                                style={style}
                            />
                        </MuiThemeProvider>

                        <MuiThemeProvider muiTheme={muiTheme}>
                            <RaisedButton
                                label="Забронировать"
                                primary={true}
                                style={style2}
                                onClick={this.handleOpen}
                            />
                            <Dialog
                                title="Форма брони"
                                actions={actions}
                                modal={true}
                                open={this.state.open}
                            >
                                <TextField
                                    hintText="Hint Text"
                                    floatingLabelText="Имя"
                                /><br/>
                                <TextField
                                    hintText="Hint Text"
                                    floatingLabelText="Фамилия"
                                /><br/>
                                <TextField
                                    hintText="Hint Text"
                                    floatingLabelText="Телефон"
                                /><br/><br/>
                            </Dialog>
                        </MuiThemeProvider>

                    </div>

                </div>
            </div>
        )
    }
}
export default InfoOrganization;